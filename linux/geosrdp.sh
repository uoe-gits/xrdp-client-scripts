#!/bin/bash

KN=$USER@EASE.ED.AC.UK

# check if we have a valid kerberos ticket
if ! klist -s
then
    zenity --password --title "login to ease" | kinit $KN
fi

if ! klist -s
then
   zenity --error --text="no valid kerberos ticket for $KN"
   exit 1
fi

ssh -f -L 9999:xrdp.geos.ed.ac.uk:3389 $USER@ssh.geos.ed.ac.uk -N -C

exec xfreerdp /compression -themes -wallpaper /audio-mode:1 /u:$USER  /v:localhost:9999 /multimon /clipboard

