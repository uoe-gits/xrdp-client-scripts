Scripts to access GeoSciences xrdp Pool
=======================================

Linux
-----
Use ssh to create a tunnel. Assume kerberos client tools are installed. If
no valid kerberos ticket available run kinit.

Put the geosrdp.sh script into your ~/bin directory and the geoscience.desktop
file into .local/share/applications/.